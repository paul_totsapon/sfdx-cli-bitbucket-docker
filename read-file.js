const readline = require('readline');
const fs = require('fs');
const { exit } = require('process');

// create instance of readline
// each instance is associated with single input stream
let rl = readline.createInterface({
	input: fs.createReadStream(process.argv[2])
});

let complexComponentsArgv = process.argv[3] || false;

let allowedComponents = [
	'components',
	'classes',
	'pages',
	'triggers',
	'aura',
	'lwc'
];

if(complexComponentsArgv !== false && typeof complexComponentsArgv === 'string' && complexComponentsArgv.length > 0) {
	let complexArr = complexComponentsArgv.split(',');

	allowedComponents = allowedComponents.filter(function(value, index, arr){
		return complexArr.indexOf(value) === -1;
	});
}

let object_count = {};
let filesToDeployList = [];
let filesToDeleteList = [];

// event is emitted after each line
rl.on('line', function (line) {
	if (line.startsWith('M') || line.startsWith('A') || line.startsWith('D')) {
		let t = line.split('\t');
		if (t.length > 0) {
			let status = t[0];
			let file_path = t[1];

			if (status == 'M' || status == 'A') {
				if (file_path.startsWith('force-app/main/default/components/') ) {
					let tmp = file_path.replace('force-app/main/default/', '');
					let arr = tmp.split('/');
					// console.log('[' + file_path + '] ' + tmp + " = " + arr.join("|") + '\n');

					if (arr.length > 1) {
						if (!(arr[1] in object_count)) {
							object_count[arr[1]] = 0;
						}else{
							object_count[arr[1]]++;
						}

						if(allowedComponents.indexOf(arr[0]) !== -1 && filesToDeployList.indexOf('force-app/main/default/' + arr[0] + '/' + arr[1]) == -1){
							filesToDeployList.push('force-app/main/default/' + arr[0] + '/' + arr[1]);
						}
					}
				} else if (file_path == 'manifest/package.xml') {

				} else if (file_path.startsWith('force-app/main/default/classes/') ||
					file_path.startsWith('force-app/main/default/pages/') ||
					file_path.startsWith('force-app/main/default/triggers/')
				) {
					let tmp = file_path.replace('force-app/main/default/', '');
					let arr = tmp.split('/');

					if (arr.length > 1 && allowedComponents.indexOf(arr[0]) !== -1 && !file_path.endsWith('-meta.xml') && filesToDeployList.indexOf(file_path) == -1) {
						filesToDeployList.push(file_path);
					}
				} else if (file_path.startsWith('force-app/main/default/aura/') ||
					file_path.startsWith('force-app/main/default/lwc/')) {
					let tmp = file_path.replace('force-app/main/default/', '');
					let arr = tmp.split('/');
					if (arr.length > 1) {
						let component_name = 'force-app/main/default/' + arr[0] + '/' + arr[1];
						if (allowedComponents.indexOf(arr[0]) !== -1 && filesToDeployList.indexOf(component_name) == -1) {
							filesToDeployList.push(component_name);
						}
					}
				} else {

				}
			} else if (status == 'D') {
				if (file_path.startsWith('force-app/main/default/classes/') ||
					file_path.startsWith('force-app/main/default/pages/') ||
					file_path.startsWith('force-app/main/default/triggers/')
				) {
					if (!file_path.endsWith('-meta.xml') && filesToDeployList.indexOf(file_path) == -1) {
						filesToDeleteList.push(file_path);
					}
				}
			}
		}
	}
});

// end
rl.on('close', function (line) {
	if (filesToDeployList.length > 0) {
		fs.writeFileSync('changes.txt', filesToDeployList.join(','));
	}

	if (filesToDeleteList.length > 0) {
		fs.writeFileSync('deletes.txt', filesToDeleteList.join(','));
	}
});